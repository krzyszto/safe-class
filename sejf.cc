#include <algorithm>
#include <cstdlib>
#include "sejf.h"
#include <cstdint>
#include "kontroler.h"
#include <iostream>
#include <utility>
#include <stdexcept>

Sejf::Sejf(std::string text, int available_accesses) : 
	controler(this)
{
	if (available_accesses < 0)
		throw std::invalid_argument(std::string("Liczba dostepow"\
					" do Sejfu nie moze byc ujemna."));
	break_in = false;
	is_manipulated = false;
	this->available_accesses = available_accesses;
	this->text = text;
}

void Sejf::set_break_in()
{
	break_in = true;
}

void Sejf::set_is_manipulated()
{
	is_manipulated = true;
}

bool Sejf::get_break_in() const
{
	return break_in;
}

bool Sejf::get_is_manipulated() const
{
	return is_manipulated;
}

int Sejf::get_available_accesses() const
{
	return available_accesses;
}

Sejf& Sejf::operator=(const Sejf& Sejf)
{
	text = Sejf.text;
	available_accesses = available_accesses;
	controler = Sejf.controler;
	return *this;
}

int16_t Sejf::operator[](const size_t ind)
{
	if (ind >= text.size())
		return -1;

	if (available_accesses == 0)
	{
		set_break_in();
		return -1;
	}
	int16_t result = text[ind];
	available_accesses--;
	return result;
}

Sejf& Sejf::operator+=(const int x)
{
	if (x >= 0)
	{
		available_accesses += x;
		set_is_manipulated();
	}
	return *this;
}

Sejf& Sejf::operator*=(const int x)
{
	if (x > 0)
	{
		available_accesses *= x;
		set_is_manipulated();
	}
	return *this;
}

Sejf& Sejf::operator-=(const int x)
{
	if ((x >= 0) && (x <= available_accesses))
	{
		available_accesses -= x;
		set_is_manipulated();

	}
	return *this;
}

Sejf::Sejf(Sejf&& Sejf) :
	controler(this)
{
	text = std::move(Sejf.text);
	break_in = Sejf.get_break_in();
	is_manipulated = Sejf.get_is_manipulated();
	available_accesses = Sejf.get_available_accesses();
}

Sejf& Sejf::operator=(Sejf&& Sejf)
{
	text = std::move(Sejf.text);
	break_in = Sejf.get_break_in();
	is_manipulated = Sejf.get_is_manipulated();
	available_accesses = Sejf.get_available_accesses();
	return *this;
}

const Kontroler Sejf::kontroler() const
{
	return controler;
}

