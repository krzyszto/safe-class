#ifndef SEJF_H
#define SEJF_H
#include <iostream>
#include <string>
#include "kontroler.h"
#include <cstdint>
#include <utility>

/*
 * Klasa reprezentująca Sejf.
 */
class Sejf final {

	friend class Kontroler;

	private:
		/**
		 * Informacja mówiąca, czy nastąpiło włamanie.
		 */
		bool break_in;

		/**
		 * Informacja mówiąca, czy ilość dostępów była zmanipulowana.
		 */
		bool is_manipulated;

		/**
		 * Ilość pozostałych dostępów do Sejfu.
		 */
		int available_accesses;

		/**
		 * Napis przechowywany w Sejfie.
		 */
		std::string text;

		/**
		 * Obiekt kontrolera.
		 */
		Kontroler controler;

		/**
		 * Prywatny konstruktor uniemożliwiający skopiowanie Sejfu.
		 */
		Sejf(const Sejf& Sejf) = delete;

		/**
		 * Ustawia informację o próbie włamania.
		 */
		void set_break_in();

		/**
		 * Ustawia informację o próbie manipulacja.
		 */
		void set_is_manipulated();

		/**
		 * Prywatne przeciążenie operatorów, aby uniemożliwić
		 * modyfikację.
		 */
		Sejf& operator=(const Sejf& Sejf) = delete;
		void operator<<=(const Sejf& Sejf) = delete;
		void operator>>=(const Sejf& Sejf) = delete;
		void operator&=(const Sejf& Sejf) = delete;
		void operator^=(const Sejf& Sejf) = delete;
		void operator|=(const Sejf& Sejf) = delete;
		void operator%=(const Sejf& Sejf) = delete;
	public:

		/**
		 * Konstruktor przyjmujący jako argument napis, który będzie
		 * przechowywany w Sejfie, oraz być może liczbę możliwych
		 * dostępów do Sejfu.
		 */
		Sejf(std::string text, int available_accesses = 42);

		/**
		 * Operator umożliwiający dostęp do litery napisu o indeksie
		 * przekazanym w argumencie.
		 */
		int16_t operator[](const size_t ind);

		/**
		 * Operator zwiększający ilość dostępów do Sejfu o x.
		 */
		Sejf& operator+=(const int x);

		/**
		 * Operator zwiększający ilość dostępów do Sejfu x razy.
		 */
		Sejf& operator*=(const int x);

		/**
		 * Operator zmniejszający ilość dostępów do Sejfu o x.
		 */
		Sejf& operator-=(const int x);

		/**
		 * Podaje obiekt kontrolera.
		 */
		const Kontroler kontroler() const;

		/**
		 * Operator przypisania dla rvalue.
		 */
		Sejf& operator=(Sejf&& Sejf);

		/**
		 * Konstruktor kopiujący dla rvalue.
		 */
		Sejf(Sejf&& Sejf);

		/**
		 * Zwraca informację o tym, czy nastąpiło włamanie.
		 */
		bool get_break_in() const;

		/**
		 * Zwraca informacje o tym, czy nasąpiła manipulacja.
		 */
		bool get_is_manipulated() const;

		/**
		 * Zwraca liczbę dostępów do Sejfu.
		 */
		int get_available_accesses() const;

};

#endif
